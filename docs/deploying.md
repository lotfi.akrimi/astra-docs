Building & Deploying ASTRA Programs
===================================

This guide provides an overview of how to build and deploy ASTRA projects using the Maven build system. To use ASTRA with Maven, you can either create a basic maven build file and associated project structure or you can use our maven archetype to create the build file and project structure. This guide explains how to do this from the command line. At the end of the document, you will find links to guides that explain how to do this with IDEs such as Intellij and Eclipse.

What you’ll build
-----------------

You’ll build and deploy a simple Hello World ASTRA application.

What you’ll need
----------------
* About 15 minutes
* A favorite text editor or IDE
* JDK 1.8 or later
* Maven 3.3+

Creating a project from scratch
-------------------------------
In a project directory of your choosing, create the following directory structure (for example, on *nix systems, type `mkdir -p src/main/astra`):

```
|-- src
|    |-- main
|          |-- astra
|-- pom.xml
```

Now create the maven build file (pom.xml):

```
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>examples</groupId>
	<artifactId>hello</artifactId>
	<version>1.3.2</version>

    <parent>
        <groupId>com.astralanguage</groupId>
        <artifactId>astra-base</artifactId>
        <version>1.3.2</version>
    </parent>

    <build>
        <defaultGoal>clean compile astra:deploy</defaultGoal>
        <plugins>
            <plugin>
                <groupId>com.astralanguage</groupId>
                <artifactId>astra-maven-plugin</artifactId>
                <version>1.3.2</version>
            </plugin>
        </plugins>
    </build>
</project>
```

You should change the groupId and artifactId to reflect your own project.

Creating a project from an archetype
------------------------------------

To use the maven archtetype mechanism to create an ASTRA project, go to a folder of your choice, and type in the following command:

```
mvn archetype:generate \
	-DarchetypeGroupId=com.astralanguage \
	-DarchetypeArtifactId=astra-archetype \
	-DarchetypeVersion=1.0.0 \
	-DgroupId=examples \
	-DartifactId=hello \
	-Dversion=0.1.0
```

You should change the groupId and artifactId to reflect your own project.

Learn what you can do with ASTRA
--------------------------------
* ASTRA code is compiled into Java code prior to deployment by the ASTRA compiler, which can be invoked by using the `astra:compile` goal. The compiler converts each agent program (a .astra file) into a Java class that is created in the `target/gen/java` folder. Once all the agent programs have been compiled into Java classes, the ASTRA compiler then compiles the Java code with the class files being added to the `target/classes` folder.
* ASTRA code is designed to closely correspond to Java code. This includes adopting the Java package model. Programs written in the `src/main/astra` folder are considered to be in the default package. Programs written in subfolders of that folder as considered to be in named packages and, like Java, the program must include a package declaration. For example, the following program: `src/main/astra/soccer/Defender.astra` is in the `soccer` package and so should include the following declaration at the top of the agent program: `package soccer;`
* Each generated Java program includes a `main()` method that can be used to run the agent independently. This method: sets up a scheduling policy for the platform; creates an instance of the agent (with a default name: main); and gives the agent an initial goal of the form `!main(list args)` where the args parameter is a list containing any values passed to the main() method when the program is launched.
* ASTRA programs can be run using the `astra:deploy maven` goal. This goal assumes that there is an agent program with name `Main` in the default package (the `src/main/astra` folder) of the codebase. You can override this by setting the `astra.main` property in maven. For example, the code below would use the `soccer.Manager` agent program as a starting point.

```
<properties>
    <astra.main>soccer.Manager</astra.main>
</properties>

```
Other properties you can set here include:

* `astra.name` can be used to specify the name to be given to the first agent created (by default, this agent is called `main`)

Create a simple ASTRA agent
---------------------------

Now you can create, compile, and run your first agent. To make things simple, we will write an agent program called `Main.astra` that will be created in the `src/main/astra` folder.

*NOTE: If you used the archetype, then this file will be created by default for you.*

We do this because this file is the default file searched for by the ASTRA compiler plugin. We can use other filenames, but we will have to change our pom.xml to reflect this. We will give an example of this later.

`src/main/astra/Main.astra`

```
agent Main {
    module Console C;

    rule +!main(list args) {
        C.println("Hello World, ASTRA");
    }
}
```
There are a few things to note:

* The `agent` keyword is a lot like the Java class keyword, in fact, the syntax is very similar – this is done purposefully to make writing ASTRA code more familiar.
* The `agent` keyword declares a new agent program whose name/id matches the name of the file (the `Main` agent program is in the `Main.astra` file).
* The `module` statement declares that an instance of the `astra.lang.Console class` (the package is inferred just like Java infers that the `Object` class is actually `java.lang.Object`) will be created. This class implements an ASTRA API (a set of methods that implement functionality that can be used in the ASTRA program).
* The `rule` statement declares an agent behaviour. Each rule defines how an agent responds to an event in a context. Here, the event is the `+!main(...)` event which is generated when an agent program is executed (it is the ASTRA equivalent of a `main()` method). The above rule does not provide a context, meaning that the rule is always applicable. We will see an example of a context later.
* The Console API class provides a series of methods that support input/output from/to the console. In the example, the agent prints out “Hello World, ASTRA”.

Run the application
-------------------

To run the application, simply type the following command in the project root folder:

```
mvn
```

The maven build file includes a default goal that is executed. You can do the same by entering the following line:

```
mvn clean compile astra:deploy
```

While there will be a lot of output, at the end you should see:

```
[INFO]
[INFO] --- astra-maven-plugin:0.1.0:deploy (default-cli) @ hello ---
[main]Hello World, ASTRA
```

Running a “non-default” Agent Program
-------------------------------------

By default, the `astra:deploy` goal is configured to run the `Main.astra` agent program. We can override this by setting the `astra.main` property either in the maven build file or at the command prompt.

To see an example of how to do this, create a new agent program called `Namey.astra` and place it in the default package (`src/main/astra`).

`src/main/astra/Namey.astra`

```
agent Namey {
    module Console console;
    module System system;

    rule +!main(list args) {
        console.println("Hello World, "+system.name());
    }
}
```

This program differs from the previous program in 3 ways:

* The name of the agent program is `Namey` this has been changed to match the name of the file that the agent program was written in.
* We have added a second module – the `System` module.
* The `println(....)` statement uses the System module to get the name of the agent which is included in the output.

To run the program, you can either add a properties setting to the maven build file:

```
<properties>
    <astra.main>Namey</astra.main>
</properties>
```

Alternatively, you can override the default agent program name at the command line:

```
mvn -Dastra.main=Namey
```

This should result in the following output:

```
[main]Hello World, main
```

Notice that the name of the agent appears in two places. The left-hand occurrence is to enable you to know which agent is generating output. The right-hand occurrence is due to the code we wrote above.

As a second step, we can also change the name of the default agent. We do this by overriding the default value for `astra.name`. Again, we can do this in the properties section of the pom.xml file:

```
<properties>
    <astra.main>Namey</astra.main>
    <astra.name>George</astra.name>
</properties>
```

Or we can do this at the command line:

```
mvn astra:deploy -Dastra.main=Namey -Dastra.name=George
```

The output of this would be:

```
[George]Hello World, George
```

Summary
-------

Congratulations! You created and deployed your first agent using ASTRA and Maven. You have also seen how to customise the start-up process to change the main agent program and the main agent name.

However, this tells you very little about what you can do with agents, if you want to find out more, please read some of the other guides. Below, we identify the guides that we thing are the most appropriate next steps for you.

See Also
--------

The following guides may also be helpful:

* [Creating Multiple Agents with ASTRA](multiple.md)
