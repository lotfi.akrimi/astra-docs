#8. Publications

##8.1 Core Publications

* Dhaon, A., Collier R. (2014), Multiple Inheritance in AgentSpeak(L)-style Programming Languages, in Proceedings of the 4th International Workshop on Programming based on Actors, Agents and Decentralized Control (held at SPLASH 2014), Portland, Oregon, USA [pdf](publications/agere2014.pdf)
* Collier, R., Russell, S., Lillis, D. (2015) Reflecting on Programming with AgentSpeak(L), in Proceedings of 18th International Conference on Principles and Practice of Multi-Agent Systems (PRIMA2015), Bertinoro, Italy [pdf](publications/prima15.pdf)
* Collier, R., Russell, S., Lillis, D. (2015) Exploring AOP from an OOP Perspective, in Proceedings of the 5th International Workshop on Programming based on Actors, Agents and Decentralized Control (held at SPLASH 2014), Pittsburgh, Pennsylvania, USA [pdf](publications/agere15.pdf)

##8.2 AgentSpeak(ER)
* Ricci, A., Bordini, R. H., Hübner, J.F., Collier, R. (2018) AgentSpeak (ER): Enhanced encapsulation in agent plans, in International Workshop on Engineering Multi-Agent Systems, pp. 34-51. Springer, Cham [pdf](http://emas2018.dibris.unige.it/images/papers/EMAS18-02.pdf)
* Ricci, A., Bordini, R. H., Hübner, J.F., Collier, R. (2018) AgentSpeak (ER): An Extension of AgentSpeak (L) improving Encapsulation and Reasoning, in AAMAS'18 Proceedings of the 17th International Conference on Autonomous Agents and MultiAgent Systems. International Foundation for Autonomous Agents and MultiAgent Systems (IFAAMAS) [pdf](https://www.ifaamas.org/Proceedings/aamas2018/pdfs/p2054.pdf)
* Bordini, R.H., Collier, R., Hübner, J.F. and Ricci, A. (2020) Encapsulating Reactive Behaviour in Goal-Based Plans for Programming BDI Agents, in Proceedings of the 19th International Conference on Autonomous Agents and MultiAgent Systems [pdf](https://ifaamas.org/Proceedings/aamas2020/pdfs/p1783.pdf)

##8.3 Multi-Agent Microservices (MAMS)
* Collier, Rem, Eoin O'Neill, David Lillis, and Gregory O'Hare. "MAMS: Multi-Agent MicroServices✱." In Companion Proceedings of The 2019 World Wide Web Conference, pp. 655-662. 2019 [pdf](http://193.1.133.232/pubs/Collier2019.pdf)
* O'Neill E, Lillis D, O'Hare GM, Collier RW. Explicit modelling of resources for multi-agent microservices using the cartago framework. InProceedings of the 19th International Conference on Autonomous Agents and MultiAgent Systems 2020 May 5 (pp. 1957-1959) [pdf](https://www.ifaamas.org/Proceedings/aamas2020/pdfs/p1957.pdf)
* O’Neill E, Lillis D, O’Hare GM, Collier RW. Delivering multi-agent MicroServices using CArtAgO. InInternational Workshop on Engineering Multi-Agent Systems 2020 Jun 8 (pp. 1-20). Springer, Cham [pdf](https://drive.google.com/file/d/1srjfgsRZ75i3oiWfZLWOXuA-YACLxuhk/view)

##8.4 Appliations
* O'Neill, E., Beaumont, K., Bermeo, NV., Collier, RW., Building Management using the Semantic Web and Hypermedia Agents, Proceedings of the All The Agents Challenge (ATAC) @ International Semantic Web Confernece (ISWC), 2021 [pdf](http://ceur-ws.org/Vol-3111/short5.pdf)
* Beaumont, K., O'Neill, E., Bermeo, NV., Collier, RW., Building Management using the Semantic Web and Hypermedia Agents, Proceedings of the All The Agents Challenge (ATAC) @ International Semantic Web Confernece (ISWC), 2021 [pdf](http://ceur-ws.org/Vol-3111/short2.pdf)
* Collier, RW., Russell, S., Golpayegani, F., Harnessing Hypermedia MAS and Microservices to Deliver Web Scale Agent-based Simulations, Proceedings of the International Conference on Web Information Systems and Technologies (WEBIST), 2021 [pdf](https://www.scitepress.org/Papers/2021/107111/107111.pdf)
