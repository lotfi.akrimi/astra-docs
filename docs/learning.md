#5. Learning to use ASTRA

##5.1 The Basics

The guides below provide some basic techniques for writing ASTRA programs. Explanations are based on Maven command line tools, but you should be able to adapt these if you complete the relevent IDE guide above.

* [Building & Deploying ASTRA Programs](deploying.md)
* [Creating Multiple Agents with ASTRA](multiple.md)
* [Multi-Agent Communication with ASTRA](communication.md)
* [Modelling Problems in ASTRA](modelling.md)
* [Working with Lists](lists.md)
* [Multiple Inheritence](multiple-inheritence.md)

##5.2 Creating your own ASTRA Modules

Modules are the primary extension point through which you are able to add your own functionality to an ASTRA program.  This section starts by exploring how to create modules through a series of guides. The final two guides provide extensive examples of modules, however, the best source of examples is the [ASTRA APIS Library](https://gitlab.com/astra-language/astra-core/-/tree/master/astra-apis) which contains the default [libraries](libraries.md) provided with ASTRA.

* [Creating your own Actions](modules/actions.md)
* [Adding a Sensor to ASTRA](modules/sensors.md)
* [Implementing your own Events](modules/events.md)
* [Defining Custom Terms and Formulae](modules/terms.md)
* [Example - Calculator](modules/calculator.md)
* [Example - Implementing GUI support](modules/guis.md)

##5.3 Working with EIS Environments

The [Environment Interface Standard (EIS)](https://github.com/eishub) is an attempt to provide a common interface between agents and the environments the inhabit. The idea of the initiative is to standardise how an agent language interacts with an environment. This means that language developers need only to link their programming language to the interface and then they have access to all of the environments that have been developed for that interface.

For ASTRA, the approach that has been taken to linking agents to the interface has been based on a core language level integration. This support includes a statement for executing actions in the environment, a formula for querying environment beliefs, and an event for triggering behaviours. Additionally, a custom API has been developed to provide additional support for working with EIS environments.

* [Overview of the EIS module](eis/index.md)

##5.4 ASTRA By Example

This part of the guide presents a series of simple programs that illustrate how various language features are implemented.

* [Basic ASTRA](basicastra.md)

