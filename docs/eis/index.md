Overview of the EIS Module
==========================

This guide provides a general overview of the EIS module that comes with ASTRA. The guide is tailored to Version 1.2.1 of ASTRA (and later).  Earlier versions of ASTRA included EIS support, but it was slightly different.

**Note: In order to work with EIS environments, you must have some idea about how ASTRA modules work.**

What is EIS?
------------

EIS stands for Environment Interface Standard. It is a general purpose interface for linking environments to agents. EIS is an integration architecture that provides three standardised APIS: one API for agents that defines how to interact with an envrionment, one API for envrionments that exposes the state and functionality of the environment in a consistent way, and one management API for setting up and controlling the operation of an envrionment. EIS environments are packaged as JAR files for portability.  Agent Platform developers are required to integrate their platform with the agent and management APIS. This allows users of the agent platform to load and execute any of the available environments.  A repository of existing environments is available at [EISHUB](https://github.com/eishub).

**Note: Due to its "agent-first" philosophy, ASTRA combines the agent and management APIs into a single module.**

Briefly, EIS environments consist of a set of entities (things that are controllable by agents). Once an environment has been deployed, agents can be registerd with it. A registered agent can be linked to one or more entities in the environment.  Once linked, the agent begins to receive perceptions about the state of the environment from the entity and is able to make the entity perform associated actions.

Launching an EIS Environment
----------------------------

To launch an EIS environment, you need to create an agent with an instance of the EIS module.  The environment jar file must be located in a folder that can be identified relative to your project root folder. You can then create an instance of the environment via the launch action. Some sample code is provided below:

```
agent Example {
    module EIS ei;

    rule +!main(list args) {
        ei.launch("my_id", "/path/to/MyEnvironment.jar");
    }
}
```

Notice the definition of the EIS module and the use of the launch action in the `!main(...)` rule.  The launch action takes two parameters: the first is a unique identifier for the environment (this is made up by you), and the second is the relative path to the jar file containing the environment you want to run.

Only one agent should launch an environment.  That agent is automatically registered to the environment and receives any management level events that occur. We discuss the types of event later in this guide. Other agents that you wish to connect to the environment should do so using the `join(...)` action which takes one parameter; the identifier of the environment.  The sample code below demonstrates this by defining an agent that can connect to the environment launced in the previous piece of code.

```
agent Example2 {
    module EIS ei;

    rule +!main(list args) {
        ei.join("my_id");
    }
}
```

**Note: any number of agents can be connected to an environment.  Once connected, they receive management level events. To receive entity level events, they must be linked to an entity.**

Note: EIS and Maven
-------------------

ASTRA uses Maven to build and run ASTRA code. [EISHUB](https://github.com/eishub) also provides Maven artifacts for each of its environments. To simplify the deployment of EISHUB environments, a base Maven project has been provided.  An example POM file is given below:

```
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
    <groupId>guides</groupId>
    <artifactId>astra-eis-vac</artifactId>
    <version>1.3.2</version>

    <parent>
        <groupId>com.astralanguage</groupId>
        <artifactId>astra-eis-base</artifactId>
        <version>1.3.2</version>
    </parent>

    <properties>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.source>1.8</maven.compiler.source>
        <eis.groupId>eishub</eis.groupId>
        <eis.artifactId>vacuumworld</eis.artifactId>
        <eis.version>1.2.0</eis.version>
        <eis.base>0.5.0</eis.base>
    </properties>
   
    <build>
        <defaultGoal>clean compile dependency:copy-dependencies astra:deploy</defaultGoal>
        <plugins>
            <plugin>
                <groupId>com.astralanguage</groupId>
                <artifactId>astra-maven-plugin</artifactId>
                <version>1.3.2</version>
            </plugin>
        </plugins>
    </build>
</project>
```

The `eis.groupId`, `eis.artifactId` and the `eis.version` properties are used to specify the EISHUB artifact related to the environment you want to load and the `maven-dependency-plugin` is used to copy all the jar files from all dependencies into the `target/dependency` folder.  The relevant environment jar file can then be identified based on this. For example, the above pom file copies the vacuumworld-1.2.0.jar file into the dependency folder.  This jar file should then be referenced in the ASTRA code by the path `dependency/vacuumworld-1.2.0.jar`.  A modified version of the ASTRA example that launches that environment specified in the above POM file is provided below:

```
agent Example {
    module EIS ei;

    rule +!main(list args) {
        ei.launch("my_id", "dependency/vacuumworld-1.2.0.jar");
    }
}
```

Management Functionality
------------------------

