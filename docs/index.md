# ASTRA Programming Language

## Welcome to the ASTRA documentation

### Introduction

ASTRA stands for AgentSpeak(TR) Agents and is an Agent-Oriented Programming Language developed by researchers at [University College Dublin](http://www.ucd.ie). It is a combination of the AgentSpeak(L) programming language proposed by Arnund Rao in 1996 and the Teleo Reactive programming language created by Nils Nilson in 1994.  It is designed to be familiar to programmers of C-style languages and is built on the Java Programming Language.

ASTRA is free to use under the [MIT License](https://gitlab.com/astra-language/astra-core/-/blob/master/LICENSE).

### A Simple Example

ASTRA programs consist of a set of *agent classes*. Each agent class is written in a separate file whose name must match the name of the agent class and which must have a `.astra` extension. For example, the program below would be written in a file called `Hello.astra`

```
agent Hello {
    module Console console;

    rule +!main(list args) {
        console.println("Hello World!");
    }
}
```

This program consists of two statements: a module declaration and a rule.  The module declaration provides a mechanism to import libraries into ASTRA programs.  This particular library (the `Console` library) provides support for reading from or writing to the system console.  Libraries are implemented as Java classes, and the module declaration results int the creation of an instance of that class for exclusive use by the agent.  Multiple instances of a module are allowed and individual instances are distingished by an identifier (here `console` - note the lower case).

An example of a module in use can be seen in the rule statement. AgentSpeak(L) (and ASTRA) is event-driven. The agent acts in response to events. Rules map events to plans that should be adopted should the agent be required to handle the event.  In the above example, the event that the rule is mapped to is `+!main(list args)`. This is called the *main* event and it is the ASTRA equivalent to the main method in Java (the argument is equivalent to the array of String objects declared in the main method).   The associated plan contains a single statement that prints out `Hello World` to the console.

## Getting ASTRA

ASTRA requires no direct installation. Instead, it relies on the [Maven Build System](http://maven.apache.org).  All ASTRA libraries are available through [Maven Central](https://search.maven.org/search?q=g:com.astralanguage).

For details on how to install Maven, please read the [Apache Maven Documentation](https://maven.apache.org/install.html).

## Contributing

ASTRA is an open source project. We are always very happy to work with others interested in extending and improving it. The [source code](https://gitlab.com/astra-language) is available on Gitlab.

If you want to get involved, please give me a shout: [rem.collier@ucd.ie](mailto:rem.collier@ucd.ie)
